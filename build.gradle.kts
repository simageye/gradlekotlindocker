
import org.springframework.boot.gradle.tasks.bundling.BootJar
import org.springframework.boot.gradle.tasks.run.BootRun

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:2.1.2.RELEASE")
		// For dockerizing
        classpath("gradle.plugin.com.palantir.gradle.docker:gradle-docker:0.21.0")
    }
}



plugins {
	java
	eclipse
	idea
	id("org.springframework.boot") version "2.1.3.RELEASE"
	id("io.spring.dependency-management") version "1.0.7.RELEASE"
	id("com.palantir.docker") version "0.21.0"
}


/*** My Task jar file ***/
Tasks.named<BootJar>("bootJar") {
	baseName = "spring-boot-docker"
	version = "0.1.0"
}


Repositories {
	mavenCentral()
}


Tasks.withType<ScalaCompile> {
	sourceCompatibility = "1.8"
}


Dependencies {
    compile("org.springframework.boot:spring-boot-starter-web")
    testCompile("org.springframework.boot:spring-boot-starter-test")
	//compile(kotlin("stdlib-jdk8"))
}


/*** Own defined gradle tasks ***/
//[project.]task<CLASS>(TASKNAME) {...}
Task("printSomething") {
	group = "MGE"
	description = "#"
	doFirst {
		println("The project name is: ${project.name}")
	}
	doLast {
		println("CREATOR...so last")
	}
}



//for including in the copy task
val dataContent = copySpec {
    from("src/data")
    include("*.data")
}
tasks {
    register("***Own_Tasks***", Copy::class) {

        val tokens = mapOf("version" to "0.0.1")
        inputs.properties(tokens)

        from("src/main/config") {
            include("**/*.properties")
            include("**/*.xml")
            filter<ReplaceTokens>("tokens" to tokens)
        }

        from("src/main/languages") {
            rename("EN_US_(.*)", "$1")
        }

        into("build/target/config")
        exclude("**/*.bak")
        includeEmptyDirs = false
        with(dataContent)
    }
    register("clean", Delete::class) {
        delete(buildDir)
    }
}

// For dockerizing subtask
task<Copy>("unpack") {
	group = "MGE"
	description = "#"
    dependsOn("bootJar")
	//from(zipTree("build/libs/${bootJar.archiveName}"))
    //from(zipTree(tasks.bootJar.outputs.files.singleFile))
    //from(zipTree(tasks.bootJar.outputs.files.singleFile))
    //into("build/dependency")
}
 For dockerizing maintask
docker {
    name("${project.group}/${bootJar.baseName}"
  //copySpec.from(tasks.unpack.outputs).into("dependency")
  //buildArgs(['DEPENDENCY': "dependency"])
}
/*** *** ***/
