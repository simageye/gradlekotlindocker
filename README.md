# gradleKotlinDocker

gradle_Kotlin_Docker Template

______
# Dockerizing
We added a **VOLUME** pointing to *"/tmp"* **because that is where a Spring Boot application creates
working directories for Tomcat by default**. The effect is to create a temporary file on your host
under *"/var/lib/docker"* and link it to the container under *"/tmp"*. This step is optional for the
simple app that we wrote here, but can be necessary for other Spring Boot applications if they need
to actually write in the filesystem.<br/>
<br/>
To reduce Tomcat startup time we added a system property pointing to *"/dev/urandom"* as a source of
entropy. This is not necessary with more recent versions of Spring Boot, if you use the *"standard"*
version of Tomcat (or any other web server).

______
# Commands until running
./gradlew builddocker <br/>
docker run -p 7000:8080 -t --name tpsospringboot DOCKERREGISTRY/tpso/tpso-spring-graylog-java-framework:0.1.0

______
### Quellen
https://daringfireball.net/projects/markdown/syntax#hr <br/>
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet <br/>
<br/>
Springboot:<br/>

https://spring.io/guides/gs/spring-boot-docker/#scratch<br/>
https://spring.io/projects/spring-boot#learn<br/>
https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-customize-the-whitelabel-error-page<br/>
https://www.baeldung.com/spring-boot-context-path<br/>
https://docs.spring.io/spring-boot/docs/2.2.0.RELEASE/reference/html/appendix-application-properties.html#common-application-properties<br/>
https://javadeveloperzone.com/spring-boot/spring-boot-location-of-application-properties-or-yml-file/#1_applicationproperties_file_should_be_inconfigsubdirectory_of_the_current_directory<br/>
https://howtodoinjava.com/spring-boot2/logging/logging-application-properties/<br/>
<br/>
Java:<br/>
https://www.sonntag.cc/teaching/JAVA-Kurs/appendices/Schluesselwoerter.htm<br/>
https://www.tutorialspoint.com/java/java_strings.htm<br/>

______
### Clean dir tree

```Bash
.
├── build.gradle
├── build.gradle.kts
├── docker
│   ├── spring
│   │   └── Dockerfile
│   └── spring_data
├── Dockerfile -> docker/spring/Dockerfile
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
├── maintenance
├── Makefile
├── README.md
└── src
    └── main
        └── java
            └── appli
                └── Application.java
```
