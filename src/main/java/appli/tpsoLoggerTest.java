
package appli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

/* rbb cmsevo graylog libraries */
/* https://docs.rbb-online.de/wiki/display/evo/Logging+-+Benutzung+des+cmsevo-Log-Frameworks */
import de.rbb.cmsevo.commons.log.Logger;
import de.rbb.cmsevo.commons.log.LoggerFactory;
import de.rbb.cmsevo.commons.log.StructuredLogger.LoggerContext;



@SpringBootApplication
@RestController
public class tpsoLoggerTest {
	
	@RequestMapping("/")
	public String home() {
		execute();
		return "Springboot container for logging test from tpso here.";
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(tpsoLoggerTest.class);
    
    private enum LogFields {
        ID1,
        ID2,
        ID3,
        DATA;
         
        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }
	
    @Test
    public void execute() {
        try (LoggerContext lc = LOGGER.addContext(
                LogFields.ID1, "foo",
                LogFields.ID2, "bar",
                LogFields.DATA, new Date())) {
 
            LOGGER.debug("executing (#1)...");
 
            Map<String, Object>   logMsg = new HashMap<>();
            logMsg.put("msg", "logging via Map");      
            logMsg.put("key", "value");    
            logMsg.put("key2", "value2");
            LOGGER.debug(logMsg);
             
            // Verschachtelter Kontext
            try (LoggerContext lc2 = LOGGER.addContext(
                    LogFields.ID2, "baz",
                    LogFields.ID3, "fooo")) {
                LOGGER.debug("executing (#2)...");
            }
             
            Thread.sleep(1000);
            LOGGER.jdebug("jdebug-Message", "now", new Date(), LogFields.ID1, "id1-override");
 
            throw new RuntimeException("Test-Exception");
 
        } catch (Exception e) {
            LOGGER.error("Exception occurred:", e);
        }
    }
      
    public static void main(String[] args) {
        SpringApplication.run(tpsoLoggerTest.class, args);
    }
}
