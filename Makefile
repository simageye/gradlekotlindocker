# PHONY := target is not associated with a physical file 
## (e.g.: the target "clean" is independent from the "file clean", if it exists)
.PHONY: all initBuild build clean info showOwnTasks specificTask resetHard


### Define Variables ###
# --- Project Variables ---
PROJECTNAME=appli

# --- Must have Variables ---
ROOTDIR:=$(shell pwd)
G=gradle
GW=./gradlew
GGWOPTS=-i
GGWOPTSC=--build-cache

JAVA=java
JAVAOPTS=-jar

# --- java specific ---
SRCPATH=src/main/java/${PROJECTNAME}
	# Projectname (DIR) == package name in java-code
	# Projectname (DIR)/Application.java 
# --- --- ---

#touch .project ## Is not necessary anymore for configure-->converting into Gradle STS
## because of "eclipseProject"

### ### ###

### Makefile Logic ###
default: showOwnTasks


all: resetHard initBuild assemble build builddocker  


resetHard:
	rm -rf ${G}
	rm -rf ${GW}*
	rm -rf ${ROOTDIR}/build
	## Optional:
	#rm -rf ${ROOTDIR}/src

initBuild:
	${G} ${GGWOPTS} wrapper     # Get wrapper libs
	mkdir -p ${SRCPATH} 



assemble: # Downloading dependencies and compile java code
	${GW} ${GGWOPTS} assemble #${GGWOPTSC} --stacktrace --debug 
	## Optional:
	#${GW} ${GGWOPTS} eclipseProject
	
assemblejar:
	${GW} ${GGWOPTS} eclipse


runSpringboot:
	${GW} ${GGWOPTS} bootRun
	## Alternative: ./gradlew build && java -jar build/libs/gs-spring-boot-docker-0.1.0.jar



clean: 
	rm -rf ${ROOTDIR}/build/classes

cleanAll:
	${G} ${GGWOPTS} clean     # rm ./build directory
	## Optional:
	#${G} ${GGWOPTS} cleanEclipse



# --- Testing ---
build: 
	${GW} ${GGWOPTS} build    # assemble & test

test:     # Test from scratch
	${GW} ${GGWOPTS} clean
	${GW} ${GGWOPTS} test     # Run Unit-tests

builddocker:
	${GW} ${GGWOPTS} build docker  # create dockerimage

# --- Information & Specials ---
info:
	${GW} ${GGWOPTS} properties
	${GW} ${GGWOPTS} tasks 

showOwnTasks:
	${GW} ${GGWOPTS} -q tasks --all | grep --color=always -A 100 'MGE tasks' | grep --color=always '#'

specificTask:
	${GW} ${GGWOPTS} printSomething
	#${G} ${GGWOPTS} -q tasks printSomething
	
# --- --- ---

### ### ###
